<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AccountsController;
use App\Http\Controllers\Api\OrdersController;

Route::group([], function () {
    //Rutas de Account
    Route::get('/getacounts', [AccountsController::class, 'index']);
    Route::post('/addacount', [AccountsController::class, 'create']);
    Route::put('/updateacount/{id}', [AccountsController::class, 'update']);
    Route::delete('/deleteacount/{id}', [AccountsController::class, 'delete']);
    Route::get('/view/{id}', [AccountsController::class, 'getAccount']);
    //Fin de rutas de Accpunt

    //Inicio Rutas Order
    Route::post('/addorder', [OrdersController::class, 'newOrder']);
    Route::put('/updateorder', [OrdersController::class, 'updateOrder']);
    Route::get('/validate/{id}', [OrdersController::class, 'validateOrder']);
    Route::delete('/deleeteorder/{id}', [OrdersController::class, 'deleteOrder']);
    Route::get('/cancel/{id}', [OrdersController::class, 'cancelOrder']);
    //Fin rutas Order
});
