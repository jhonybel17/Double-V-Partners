<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>

<body>
    <div class="container py-5">
        <div class="row">
            @foreach($items as $item)
            <div class="col-lg-4 my-3">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">{{$item->Nombre}}</h3>
                        <p class="text-muted">{{$item->Email}}</p>
                        <p class="card-text">{{$item->telefono}}</p>
                        <div class="d-flex justify-content-between">
                            <small>Creado: {{$item->created_at}}</small>
                            <small>Última actualización: {{$item->updated_at}}</small>
                        </div>
                        <div class="text-center">
                            <button type="button" data-id="{{$item->_id}}" class="btn btn-primary mt-3">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-lg-4 my-3">
                <div class="card">
                    <div class="card body">
                        <div class="container mt-4">
                            <label for="Title" class="form-label ">Crear Cliente</label>
                            <form id="FormCreated" class="needs-validation" novalidate>
                                <div class="mb-3">
                                    <label for="Nombre" class="form-label">Nombre</label>
                                    <input type="text" class="form-control" id="Nombre" name="Nombre" required>
                                    <div class="invalid-feedback">Por favor, ingresa un nombre.</div>
                                </div>
                                <div class="mb-3">
                                    <label for="Email" class="form-label">Email</label>
                                    <input type="email" class="form-control" id="Email" name="Email" required>
                                    <div class="invalid-feedback">Por favor, ingresa un email válido.</div>
                                </div>
                                <div class="mb-3">
                                    <label for="telefono" class="form-label">Teléfono</label>
                                    <input type="text" class="form-control" id="telefono" name="telefono" required>
                                    <div class="invalid-feedback">Por favor, ingresa un número de teléfono.</div>
                                </div>
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.socket.io/3.1.3/socket.io.min.js" integrity="sha384-cPwlPLvBTa3sKAgddT6krw0cJat7egBga3DJepJyrLl4Q9/5WLra3rrnMcyTyOnh" crossorigin="anonymous"></script>
    <script>
        const socket = io.connect('http://192.168.1.41:3030');
        document.addEventListener('click', (e) => {
            const clickedButton = e.target.closest('button[data-id]');

            if (clickedButton) {
                const itemID = clickedButton.getAttribute('data-id');

                fetch(`http://localhost:8000/api/deleteacount/${itemID}`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(response => response.json())
                    .then(data => {
                        console.log(data.message); 
                        socket.emit('Cliente Eliminado', {
                            Cliente: itemID
                        });
                        window.location.reload();
                    })
                    .catch(error => {
                        console.error('Error:', error);
                    });
            }
        });
    </script>
    <script>
        document.getElementById('FormCreated').addEventListener('submit', function(event) {
            event.preventDefault();

            if (this.checkValidity()) {
                const formData = new FormData(this);

                const jsonData = {};
                formData.forEach((value, key) => {
                    jsonData[key] = value;
                });

                fetch('/api/addacount', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(jsonData)
                    })
                    .then(response => response.json())
                    .then(data => {
                        socket.emit('Cliente Registrado', {
                            "Cliente Registrado": data["Registro Almacenado"]
                        });
                        window.location.reload();
                    })
                    .catch(error => {
                        console.error('Error:', error);
                    });
            }

            this.classList.add('was-validated');
        });
    </script>
</body>

</html>