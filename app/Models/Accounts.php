<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Accounts extends Model
{
    use HasFactory;
    protected $connection = "mongodb";

    protected $fillable = [
        'Nombre',
        'Email',
        'telefono'
    ];
}
