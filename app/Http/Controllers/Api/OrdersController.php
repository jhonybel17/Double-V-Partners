<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Orders;
use App\Http\Controllers\Api\AccountsController;

class OrdersController extends Controller
{
    protected $accountsController;

    public function __construct(AccountsController $accountsController)
    {
        $this->accountsController = $accountsController;
    }
    private function index()
    {
        $orders = Orders::all();
        return response()->json([
            "Orders Creadas" => $orders
        ]);
    }
    public function newOrder(Request $request)
    {
        $accountExists = $this->validateAccount($request->idCuenta);

        if ($accountExists) {
            $requiredVariables = ['idCuenta',  'Producto', 'Cantidad', 'Valor', 'Total'];
            $errorResponse = $this->validarVariables($request, $requiredVariables);

            if ($errorResponse !== null) {
                return $errorResponse;
            }

            $request->validate([
                'idCuenta' => 'required',
                'Producto' => 'required',
                'Cantidad' => 'required',
                'Valor' => 'required',
                'Total' => 'required',
            ]);

            $order = new Orders([
                'idCuenta' => $request->idCuenta,
                'Producto' => $request->Producto,
                'Cantidad' => $request->Cantidad,
                'Valor' => $request->Valor,
                'Total' => $request->Total,
                'Estado' => 'Activo'
            ]);

            $order->save();

            return response()->json(["Registro Almacenado" => $order], 200);
        } else {
            return response()->json(["Error" => "La cuenta que se quiere asociar no existe"], 400);
        }
    }


    public function updateOrder(Request $request)
    {
        $orderExists = $this->validateOrder($request->idPedido);

        if (!$orderExists) {
            return response()->json(["Error" => "El pedido a actualizar no existe"], 400);
        }

        $accountExists = $this->validateAccount($request->idCuenta);

        if (!$accountExists) {
            return response()->json(["Error" => "La cuenta que se quiere asociar en la actualización no existe"], 400);
        }

        $requiredVariables = ['idCuenta', 'Producto', 'Cantidad', 'Valor', 'Total'];
        $errorResponse = $this->validarVariables($request, $requiredVariables);

        if ($errorResponse !== null) {
            return $errorResponse;
        }

        $request->validate([
            'idCuenta' => 'required',
            'Producto' => 'required',
            'Cantidad' => 'required',
            'Valor' => 'required',
            'Total' => 'required'
        ]);

        $order = Orders::findOrFail($request->idPedido);
        $order->update([
            'idCuenta' => $request->idCuenta,
            'Producto' => $request->Producto,
            'Cantidad' => $request->Cantidad,
            'Valor' => $request->Valor,
            'Total' => $request->Total,
        ]);

        return response()->json(["Pedido modificado" => $order], 200);
    }

    public function deleteOrder($idOrder){
        $orderExists = $this->validateOrder($idOrder);

        if (!$orderExists) {
            return response()->json(["Error" => "El pedido a eliminar no existe"], 400);
        }
        $count = Orders::destroy($idOrder);
        return response()->json(["Registro eliminado"], 200);
    }

    public function cancelOrder($idOrder){
        $orderExists = $this->validateOrder($idOrder);

        if (!$orderExists) {
            return response()->json(["Error" => "El pedido a cancelar no existe"], 400);
        }
        $order = Orders::findOrFail($idOrder);
        $order->update([
            'Estado' => 'Cancelado'
        ]);

        return response()->json(["Pedido Cancelado" => $order], 200);
    }


    private function validarVariables(Request $request, array $variables)
    {
        $messages = [];


        foreach ($variables as $variable) {
            if (!$request->has($variable)) {
                $messages[] = "Falta {$variable} en la solicitud";
            }
        }

        if (!empty($messages)) {
            return response()->json(["Error" => implode(', ', $messages)], 400);
        }

        return null;
    }

    private function validateAccount($idaccount)
    {
        $account = $this->accountsController->getAccount($idaccount);

        return sizeof($account) > 0 ? 1 : 0;
    }

    private function validateOrder($idOrder)
    {
        $order = Orders::where("_id", $idOrder)->get();
        return sizeof($order) > 0 ? 1 : 0;
    }
}
