<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Accounts;
use Symfony\Component\HttpFoundation\Response;
class AccountsController extends Controller
{
    public function index(){
        $counts = Accounts::all();
        return response()->json([
            "Accounts Creadas" => $counts
        ]);
    }

    public function viewData(){
        $counts = Accounts::all();
        return view('welcome')->with('items', $counts);
    }

    public function create(Request $request){
        $requiredVariables = ['Nombre', 'Email', 'telefono'];
        $errorResponse = $this->validarVariables($request, $requiredVariables);
        if ($errorResponse !== null) {
            return $errorResponse;
        }
        $request->validate([
            'Nombre' => 'required',
            'Email' => 'required',
            'telefono' => 'required'
        ]);
        $count = new Accounts();
        $count->Nombre = $request->Nombre;
        $count->Email = $request->Email;
        $count->telefono = $request->telefono;
        $count->save();
        return response()->json(["Registro Almacenado" => $count], 200);
    }

    public function update(Request $request){
        $requiredVariables = ['Nombre', 'Email', 'telefono'];
        $errorResponse = $this->validarVariables($request, $requiredVariables);
        if ($errorResponse !== null) {
            return $errorResponse;
        }
        $request->validate([
            'Nombre' => 'required',
            'Email' => 'required',
            'telefono' => 'required'
        ]);
        $count = Accounts::findOrFail($request->id);
        $count->Nombre = $request->Nombre;
        $count->Email = $request->Email;
        $count->telefono = $request->telefono;
        $count->update();
        return response()->json(["Registro modificado" => $count], 200);
    }

    public function delete($id){
        $count = Accounts::destroy($id);
        return response()->json(["Registro eliminado"], 200);
    }

    private function validarVariables(Request $request, array $variables) {
        $messages = [];
        
    
        foreach ($variables as $variable) {
            if (!$request->has($variable)) {
                $messages[] = "Falta {$variable} en la solicitud";
            }
        }
    
        if (!empty($messages)) {
            return response()->json(["Error" => implode(', ', $messages)], 400);
        }
    
        return null; 
    }
    public function getAccount($id){
        $account= Accounts::where("_id", $id)->get();
        return $account;
    }
}
